default_env := development
env := $(default_env)
image := $(image)
version := $(version)
cmd := $(cmd)

build:
	cd $(env) && make $(cmd) version=$(version)
