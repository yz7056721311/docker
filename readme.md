# Dockerfiles

## Build and push (`arm64` + `amd64`)

`build`
```bash
make env=<ENV> image=<IMAGE> version=<VERSION> cmd=build
```
`push`
```bash
make env=<ENV> image=<IMAGE> version=<VERSION> cmd=push
```
or `build-push`
```bash
make env=<ENV> image=<IMAGE> version=<VERSION> cmd=build-push
```

## `development` environment images

### `node`

[yerlen/node-dev](https://hub.docker.com/r/yerlen/node-dev/tags)

Versions:

`20.13 LTS`
```bash
make env=development image=node version=20.13 cmd=build-push
```

`22`
```bash
make env=development image=node version=22 cmd=build-push
```

### `mongodb`

[yerlen/mongodb-dev](https://hub.docker.com/r/yerlen/mongodb-dev/tags)

Versions:

`7.0.7`
```bash
make env=development image=mongodb version=7.0.7 cmd=build-push
```

## `production` environment images

### `node`

[yerlen/node-prod](https://hub.docker.com/r/yerlen/node-prod/tags)

Versions:

`20.13 LTS`
```bash
make env=production image=node version=20.13 cmd=build-push
```

`22`
```bash
make env=production image=node version=22 cmd=build-push
```

### `haproxy`

[yerlen/haproxy-prod](https://hub.docker.com/r/yerlen/haproxy-prod/tags)

Versions:

`1.0.0`
```bash
make env=production image=haproxy version=1.0.0 cmd=build-push
```
